import java.util.ArrayList;
import java.util.Random;

public class Main {
    Random random = new Random();
    static ArrayList<Nevera> arrayListNeveras = new ArrayList<>();

    public static void main(String[] args) {
        Main mainThis = new Main();
        arrayListNeveras.add(new Nevera("Beko", mainThis.GenerarRandomHuevos(), mainThis.GenerarPrecioRandom(), mainThis.random.nextBoolean()));
        arrayListNeveras.add(new Nevera("Philips", mainThis.GenerarRandomHuevos(), mainThis.GenerarPrecioRandom(), mainThis.random.nextBoolean()));
        arrayListNeveras.add(new Nevera("Samsung", mainThis.GenerarRandomHuevos(), mainThis.GenerarPrecioRandom(), mainThis.random.nextBoolean()));
        arrayListNeveras.add(new Nevera("LG", mainThis.GenerarRandomHuevos(), mainThis.GenerarPrecioRandom(), mainThis.random.nextBoolean()));
        arrayListNeveras.add(new Nevera("BOSCH", mainThis.GenerarRandomHuevos(), mainThis.GenerarPrecioRandom(), mainThis.random.nextBoolean()));
        arrayListNeveras.add(new Nevera("Balay", mainThis.GenerarRandomHuevos(), mainThis.GenerarPrecioRandom(), mainThis.random.nextBoolean()));
        arrayListNeveras.add(new Nevera("Smeg", mainThis.GenerarRandomHuevos(), mainThis.GenerarPrecioRandom(), mainThis.random.nextBoolean()));

        for (Nevera n : arrayListNeveras) {
            System.out.println(n);
        }
    }

    private double GenerarPrecioRandom() {
        return ((random.nextDouble() * (1000 - 400 + 1) + 400) * 100) / 100.0;
    }

    public int GenerarRandomHuevos() {
        int numeroRandom = 1;
        while (numeroRandom % 6 != 0) {
            numeroRandom = random.nextInt(25);
        }

        return numeroRandom;
    }
}